package br.com.sbtec.integracao.steps.aluno.converter;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class EtiniaCorRacaConverterTest {

	@Test
	public void testConverter() {
		assertTrue(EtniaCorRacaConverter
				.toSede(EtniaCorRacaConverter.AMARELA.sgeValue)
				.equals(EtniaCorRacaConverter.AMARELA.sedeValue));
	}
}
