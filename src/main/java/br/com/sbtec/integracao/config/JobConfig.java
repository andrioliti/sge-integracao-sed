package br.com.sbtec.integracao.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class JobConfig {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;
	
	@Bean
	public Job buildJob(
		@Qualifier("login") Step login
		, @Qualifier("aluno-step") Step aluno
	) {
		return jobBuilderFactory.get("integracao-sge-sede")
				.incrementer(new RunIdIncrementer())
				.start(login)
				.next(aluno)
				.build();
	}
	
}
