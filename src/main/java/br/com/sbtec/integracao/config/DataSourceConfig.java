package br.com.sbtec.integracao.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class DataSourceConfig {

//	sge.datasource.driver-class=com.microsoft.sqlserver.jdbc.SQLServerDriver
//	sge.datasource.username=sa
//	sge.datasource.password=Root;12345
//	sge.datasource.host=localhost
//	sge.datasource.port=1433
//	sge.datasource.db=sge
	
	@Value("sge.datasource.driver-class")
	private String driverClass;
	
	@Value("sge.datasource.username")
	private String username;
	
	@Value("sge.datasource.password")
	private String password;
	
	@Value("sge.datasource.host")
	private String host;
	
	@Value("sge.datasource.port")
	private String port;
	
	@Value("sge.datasource.db")
	private String db;
	
//	@Bean
//	public DataSource getDataSource() {
//		String url = new StringBuilder("jdbc:sqlserver://")
//				.append(host).append(":").append(port)
//				.append(";databaseName=").append(db)
//				.toString();
//		
//		System.out.println(driverClass);
//		
//		return DataSourceBuilder.create()
//				.url(url)
//				.driverClassName(driverClass)
//				.username(username)
//				.password(password)
//				.build();
//	}
	
	@Bean
	public DataSource getDataSource() {
		String url = new StringBuilder("jdbc:sqlserver://")
				.append("localhost").append(":").append("1433")
				.append(";databaseName=").append("sge")
				.toString();
		
		return DataSourceBuilder.create()
				.url(url)
				.driverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver")
				.username("sa")
				.password("Root;12345")
				.build();
	}
	
	@Bean
	public JdbcTemplate getTemplate(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}
}
