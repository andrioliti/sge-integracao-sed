package br.com.sbtec.integracao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import kong.unirest.Unirest;

@SpringBootApplication
public class IntegracaoApplication {

	public static void main(String[] args) {
		Unirest.config().defaultBaseUrl("https://homologacaointegracaosed.educacao.sp.gov.br/ncaapi/api");
		
		SpringApplication.run(IntegracaoApplication.class, args);
	}

}
