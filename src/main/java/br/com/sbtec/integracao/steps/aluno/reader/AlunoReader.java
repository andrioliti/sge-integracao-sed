package br.com.sbtec.integracao.steps.aluno.reader;

import javax.sql.DataSource;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.sbtec.integracao.steps.aluno.dto.AlunoSGE;
import br.com.sbtec.integracao.steps.aluno.rowmapper.AlunoRowMapper;

@Configuration
public class AlunoReader  {

	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private AlunoRowMapper mapper;
	
	@Bean
	public ItemReader<AlunoSGE> getReader() {
		return new JdbcCursorItemReaderBuilder<AlunoSGE>()
					.dataSource(dataSource)
					.name("alunoItemReader")
					.sql(AlunoSgeSelect.SELECT)
					.rowMapper(mapper)
					.build();
	}

}
