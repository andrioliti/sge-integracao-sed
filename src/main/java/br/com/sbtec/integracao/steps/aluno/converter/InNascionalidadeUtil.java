package br.com.sbtec.integracao.steps.aluno.converter;

public abstract class InNascionalidadeUtil {
	
	public static String inNacional = "1";
	
	public static String inNacionalidade(Long inCodPaisOrigem) {
		if (inCodPaisOrigem == null || inCodPaisOrigem == 1l) {
			return "1";
		}
		
		return "2";
	}
}
