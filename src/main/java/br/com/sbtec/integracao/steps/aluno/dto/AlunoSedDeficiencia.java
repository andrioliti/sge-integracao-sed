package br.com.sbtec.integracao.steps.aluno.dto;

public class AlunoSedDeficiencia {
	private String inCodNecessidade;
	private Integer inMobilidadeReduzida;
	private String inTipoMobilidadeReduzida;
	private Integer inCuidador;
	private String inTipoCuidador;
	private Integer inProfSaude;
	private String inTipoProfSaude;
	
	public static AlunoSedDeficiencia fromSge(AlunoSGE sge) {
		AlunoSedDeficiencia alunoSedDeficiencia = new AlunoSedDeficiencia();
		
		if (!sge.getListNecessidadeEspecial().isBlank()) {
			alunoSedDeficiencia.setInCodNecessidade(sge.getSedListNecessidadeEspecial());
			
			if (sge.getListNecessidadeEspecial().contains("3")) {
				alunoSedDeficiencia.setInMobilidadeReduzida(NaoSimEnum.NAO.ordinal());
			}
		}
		
		return alunoSedDeficiencia;
	}
	
	public String getInTipoMobilidadeReduzida() {
		return inTipoMobilidadeReduzida;
	}

	public void setInTipoMobilidadeReduzida(String inTipoMobilidadeReduzida) {
		this.inTipoMobilidadeReduzida = inTipoMobilidadeReduzida;
	}

	public String getInCodNecessidade() {
		return inCodNecessidade;
	}

	public void setInCodNecessidade(String inCodNecessidade) {
		this.inCodNecessidade = inCodNecessidade;
	}

	public Integer getInMobilidadeReduzida() {
		return inMobilidadeReduzida;
	}

	public void setInMobilidadeReduzida(Integer inMobilidadeReduzida) {
		this.inMobilidadeReduzida = inMobilidadeReduzida;
	}

	public Integer getInCuidador() {
		return inCuidador;
	}

	public void setInCuidador(Integer inCuidador) {
		this.inCuidador = inCuidador;
	}

	public String getInTipoCuidador() {
		return inTipoCuidador;
	}

	public void setInTipoCuidador(String inTipoCuidador) {
		this.inTipoCuidador = inTipoCuidador;
	}

	public Integer getInProfSaude() {
		return inProfSaude;
	}

	public void setInProfSaude(Integer inProfSaude) {
		this.inProfSaude = inProfSaude;
	}

	public String getInTipoProfSaude() {
		return inTipoProfSaude;
	}

	public void setInTipoProfSaude(String inTipoProfSaude) {
		this.inTipoProfSaude = inTipoProfSaude;
	}

}
