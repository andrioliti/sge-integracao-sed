package br.com.sbtec.integracao.steps.aluno.converter;

import java.util.Arrays;

public enum EtniaCorRacaConverter {
	BRANCA(2, "1"),
	PRETA(5, "2"),
	PARDA(4,"3"),
	AMARELA(1, "4"),
	INDIGENA(3, "5"),
	NAO_DECLARADA(0, "6");
	
	Integer sgeValue;
	String sedeValue;
	
	EtniaCorRacaConverter(Integer sge, String sede) {
		this.sgeValue = sge;
		this.sedeValue = sede;
	}
	
	public static String toSede(Integer sge) {
		return Arrays.asList(EtniaCorRacaConverter.values()).stream()
			.filter((EtniaCorRacaConverter it) -> it.sgeValue.equals(sge))
			.findFirst()
			.orElse(EtniaCorRacaConverter.NAO_DECLARADA).sedeValue;
	}
	
}
