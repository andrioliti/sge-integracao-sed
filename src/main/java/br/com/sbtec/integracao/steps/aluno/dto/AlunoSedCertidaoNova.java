package br.com.sbtec.integracao.steps.aluno.dto;

public class AlunoSedCertidaoNova {
	private String inCertMatr01;
	private String inCertMatr02;
	private String inCertMatr03;
	private String inCertMatr04;
	private String inCertMatr05;
	private String inCertMatr06;
	private String inCertMatr07;
	private String inCertMatr08;
	private String inCertMatr09;
	private String inDataEmissaoCertidao;

	public static AlunoSedCertidaoNova fromSge(AlunoSGE sge) {
		return null;
	}
	
	public String getInCertMatr01() {
		return inCertMatr01;
	}

	public void setInCertMatr01(String inCertMatr01) {
		this.inCertMatr01 = inCertMatr01;
	}

	public String getInCertMatr02() {
		return inCertMatr02;
	}

	public void setInCertMatr02(String inCertMatr02) {
		this.inCertMatr02 = inCertMatr02;
	}

	public String getInCertMatr03() {
		return inCertMatr03;
	}

	public void setInCertMatr03(String inCertMatr03) {
		this.inCertMatr03 = inCertMatr03;
	}

	public String getInCertMatr04() {
		return inCertMatr04;
	}

	public void setInCertMatr04(String inCertMatr04) {
		this.inCertMatr04 = inCertMatr04;
	}

	public String getInCertMatr05() {
		return inCertMatr05;
	}

	public void setInCertMatr05(String inCertMatr05) {
		this.inCertMatr05 = inCertMatr05;
	}

	public String getInCertMatr06() {
		return inCertMatr06;
	}

	public void setInCertMatr06(String inCertMatr06) {
		this.inCertMatr06 = inCertMatr06;
	}

	public String getInCertMatr07() {
		return inCertMatr07;
	}

	public void setInCertMatr07(String inCertMatr07) {
		this.inCertMatr07 = inCertMatr07;
	}

	public String getInCertMatr08() {
		return inCertMatr08;
	}

	public void setInCertMatr08(String inCertMatr08) {
		this.inCertMatr08 = inCertMatr08;
	}

	public String getInCertMatr09() {
		return inCertMatr09;
	}

	public void setInCertMatr09(String inCertMatr09) {
		this.inCertMatr09 = inCertMatr09;
	}

	public String getInDataEmissaoCertidao() {
		return inDataEmissaoCertidao;
	}

	public void setInDataEmissaoCertidao(String inDataEmissaoCertidao) {
		this.inDataEmissaoCertidao = inDataEmissaoCertidao;
	}

}
