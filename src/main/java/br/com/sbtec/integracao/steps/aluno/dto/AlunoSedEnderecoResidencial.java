package br.com.sbtec.integracao.steps.aluno.dto;

public class AlunoSedEnderecoResidencial {
	private String inLogradouro;
	private String inNumero;
	private String inBairro;
	private String inNomeCidade;
	private String inUFCidade;
	private String inComplemento;
	private String inCep;
	private String inAreaLogradouro;
	private String inCodLocalizacaoDiferenciada;
	private String inCodMunicipioDNE;
	private String inLatitude;
	private String inLongitude;
	
	public static AlunoSedEnderecoResidencial fromSge(AlunoSGE sge) {
		AlunoSedEnderecoResidencial end = new AlunoSedEnderecoResidencial();
		
		end.setInLogradouro(sge.getNoLogradouro());
		end.setInNumero(sge.getNuLogradouro());
		end.setInAreaLogradouro("0");
		end.setInBairro(sge.getNoBairro());
		end.setInCep(sge.getNuCep());
		end.setInNomeCidade(sge.getNoLocalidade());
		end.setInUFCidade(sge.getNoUf());
		end.setInLatitude("0000000");
		end.setInLongitude("0000000");
		
		return end;
		
	}
	
	public String getInLogradouro() {
		return inLogradouro;
	}

	public void setInLogradouro(String inLogradouro) {
		this.inLogradouro = inLogradouro;
	}

	public String getInNumero() {
		return inNumero;
	}

	public void setInNumero(String inNumero) {
		this.inNumero = inNumero;
	}

	public String getInBairro() {
		return inBairro;
	}

	public void setInBairro(String inBairro) {
		this.inBairro = inBairro;
	}

	public String getInNomeCidade() {
		return inNomeCidade;
	}

	public void setInNomeCidade(String inNomeCidade) {
		this.inNomeCidade = inNomeCidade;
	}

	public String getInUFCidade() {
		return inUFCidade;
	}

	public void setInUFCidade(String inUFCidade) {
		this.inUFCidade = inUFCidade;
	}

	public String getInComplemento() {
		return inComplemento;
	}

	public void setInComplemento(String inComplemento) {
		this.inComplemento = inComplemento;
	}

	public String getInCep() {
		return inCep;
	}

	public void setInCep(String inCep) {
		this.inCep = inCep;
	}

	public String getInAreaLogradouro() {
		return inAreaLogradouro;
	}

	public void setInAreaLogradouro(String inAreaLogradouro) {
		this.inAreaLogradouro = inAreaLogradouro;
	}

	public String getInCodLocalizacaoDiferenciada() {
		return inCodLocalizacaoDiferenciada;
	}

	public void setInCodLocalizacaoDiferenciada(String inCodLocalizacaoDiferenciada) {
		this.inCodLocalizacaoDiferenciada = inCodLocalizacaoDiferenciada;
	}

	public String getInCodMunicipioDNE() {
		return inCodMunicipioDNE;
	}

	public void setInCodMunicipioDNE(String inCodMunicipioDNE) {
		this.inCodMunicipioDNE = inCodMunicipioDNE;
	}

	public String getInLatitude() {
		return inLatitude;
	}

	public void setInLatitude(String inLatitude) {
		this.inLatitude = inLatitude;
	}

	public String getInLongitude() {
		return inLongitude;
	}

	public void setInLongitude(String inLongitude) {
		this.inLongitude = inLongitude;
	}

}
