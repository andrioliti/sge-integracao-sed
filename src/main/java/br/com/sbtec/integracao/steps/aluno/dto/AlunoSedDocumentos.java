package br.com.sbtec.integracao.steps.aluno.dto;

import br.com.sbtec.integracao.shared.DateUtil;
import br.com.sbtec.integracao.shared.RegistroCivilUtil;

public class AlunoSedDocumentos {
	private String inNumDoctoCivil;
	private String inDigitoDoctoCivil;
	private String inUFDoctoCivil;
	private String inDataEmissaoDoctoCivil;
	private String inNumNIS;
	private String inCodigoINEP;
	private String inCPF;
	private String inJustificativaDocumentos;
	
	protected Boolean isJustificativa() {
		return (this.inNumNIS == null || this.inNumNIS.isBlank()) 
				&& (this.inCPF == null || this.inCPF.isBlank()); 
//		Justificativa para a falta de
//		documentos (Certidão Nova, CPF e
//		NIS):
	}
	
	public static AlunoSedDocumentos fromSge(AlunoSGE sge, AlunoSedCertidaoNova certidao) {
		AlunoSedDocumentos alunoSedDocumentos = new AlunoSedDocumentos();
	
		alunoSedDocumentos.setInCPF(sge.getNuCpf());
		alunoSedDocumentos.setInNumNIS(sge.getNuNis());
		alunoSedDocumentos.setInNumDoctoCivil(sge.getNuIdentidade());
		alunoSedDocumentos.setInUFDoctoCivil(RegistroCivilUtil.getUfFromEmissor(sge.getNoOrgaoEmissor()));
		alunoSedDocumentos.setInDataEmissaoDoctoCivil(DateUtil.formatDefault(sge.getDtEmissao()));
		
//		inDigitoDoctoCivil
		if (RegistroCivilUtil.isDigito(alunoSedDocumentos)) {
			//preenche o digito
		}
		
		if (alunoSedDocumentos.isJustificativa() && certidao == null) {
			//preencher com a justificativa
			alunoSedDocumentos.setInJustificativaDocumentos("2");
		}
		
		return alunoSedDocumentos;
	}

	public String getInNumDoctoCivil() {
		return inNumDoctoCivil;
	}

	public void setInNumDoctoCivil(String inNumDoctoCivil) {
		this.inNumDoctoCivil = inNumDoctoCivil;
	}

	public String getInDigitoDoctoCivil() {
		return inDigitoDoctoCivil;
	}

	public void setInDigitoDoctoCivil(String inDigitoDoctoCivil) {
		this.inDigitoDoctoCivil = inDigitoDoctoCivil;
	}

	public String getInUFDoctoCivil() {
		return inUFDoctoCivil;
	}

	public void setInUFDoctoCivil(String inUFDoctoCivil) {
		this.inUFDoctoCivil = inUFDoctoCivil;
	}

	public String getInDataEmissaoDoctoCivil() {
		return inDataEmissaoDoctoCivil;
	}

	public void setInDataEmissaoDoctoCivil(String inDataEmissaoDoctoCivil) {
		this.inDataEmissaoDoctoCivil = inDataEmissaoDoctoCivil;
	}

	public String getInNumNIS() {
		return inNumNIS;
	}

	public void setInNumNIS(String inNumNIS) {
		this.inNumNIS = inNumNIS;
	}

	public String getInCodigoINEP() {
		return inCodigoINEP;
	}

	public void setInCodigoINEP(String inCodigoINEP) {
		this.inCodigoINEP = inCodigoINEP;
	}

	public String getInCPF() {
		return inCPF;
	}

	public void setInCPF(String inCPF) {
		this.inCPF = inCPF;
	}

	public String getInJustificativaDocumentos() {
		return inJustificativaDocumentos;
	}

	public void setInJustificativaDocumentos(String inJustificativaDocumentos) {
		this.inJustificativaDocumentos = inJustificativaDocumentos;
	}

}
