package br.com.sbtec.integracao.steps.aluno.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

import br.com.sbtec.integracao.steps.aluno.converter.NecessidadesEspeciaisConverter;

public class AlunoSGE {
	public static String tableName_ = "sin_aluno";
    public static String nuMatricula_ = "nu_matricula";
    public static String coIdentificacaoAluno_ = "co_identificacao_aluno";
    public static String coPaisNacionalidade_ = "co_pais_nacionalidade";
    public static String dtMatricula_ = "dt_matricula";
    public static String dtIngresso_ = "dt_ingresso";
    public static String noAluno_ = "no_aluno";
    public static String inSexo_ = "in_sexo";
    public static String dtNascimento_ = "dt_nascimento";
    public static String edEmail_ = "ed_email";
    public static String inSituacaoAcademica_ = "in_situacao_academica";
    public static String inStatusAptoEducacaoFisica_ = "in_status_apto_educacao_fisica";
    public static String nuIdentidade_ = "nu_identidade";
    public static String noOrgaoEmissor_ = "no_orgao_emissor";
    public static String dtEmissao_ = "dt_emissao";
    public static String nuCpf_ = "nu_cpf";
    public static String nuReservista_ = "nu_reservista";
    public static String inCertidao_ = "in_certidao";
    public static String nuCertidao_ = "nu_certidao";
    public static String nuLivro_ = "nu_livro";
    public static String nuFolha_ = "nu_folha";
    public static String noCartorio_ = "no_cartorio";
    public static String edCidadeCartorio_ = "ed_cidade_cartorio";
    public static String edUfCartorio_ = "ed_uf_cartorio";
    public static String nuTituloEleitor_ = "nu_titulo_eleitor";
    public static String nuZonaEleitoral_ = "nu_zona_eleitoral";
    public static String nuSecao_ = "nu_secao";
    public static String edUfTituloEleitoral_ = "ed_uf_titulo_eleitoral";
    public static String inTipoEstadoCivil_ = "in_tipo_estado_civil";
    public static String noConjuge_ = "no_conjuge";
    public static String qtPeso_ = "qt_peso";
    public static String qtAltura_ = "qt_altura";
    public static String nuTempoEscolaridade_ = "nu_tempo_escolaridade";
    public static String nuMatriculaAntiga_ = "nu_matricula_antiga";
    public static String inStatusAluno_ = "in_status_aluno";
    public static String txSenhaAcessaInternet_ = "tx_senha_acessa_internet";
    public static String imFotoAluno_ = "im_foto_aluno";
    public static String inStatusEnsinoReligioso_ = "in_status_ensino_religioso";
    public static String txInformacoesFichaMedica_ = "tx_informacoes_ficha_medica";
    public static String coLocalidadeNaturalidade_ = "co_localidade_naturalidade";
    public static String txLocalidadeNaturalidade_ = "tx_localidade_naturalidade";
    public static String dtOperacao_ = "dt_operacao";
    public static String inOperacao_ = "in_operacao";
    public static String coPacote_ = "co_pacote";
    public static String coSeqSincronizacao_ = "co_seq_sincronizacao";
    public static String noResponsavel_ = "no_responsavel";
    public static String coEtnia_ = "co_etnia";
    public static String ufNaturalidade_ = "uf_naturalidade";
    public static String municipioNaturalidade_ = "municipio_naturalidade";
    public static String nuNis_ = "nu_nis";
    public static String noLogradouro_ = "no_logradouro";
    public static String nuLogradouro_ = "nu_logradouro";
    public static String noBairro_ = "no_bairro";
    public static String nuCep_ = "nu_cep";
    public static String noLocalidade_ = "no_localidade";
    public static String noUf_ = "no_uf";
    public static String listNecessidadeEspecial_ = "list_necessidade_especial";
    
    private String nuMatricula;
    private Long coIdentificacaoAluno;
    private Long coPaisNacionalidade;
    private Date dtMatricula;
    private Date dtIngresso;
    private String noAluno;
    private Long inSexo;
    private Date dtNascimento;
    private String edEmail;
    private Long inSituacaoAcademica;
    private Long inStatusAptoEducacaoFisica;
    private String nuIdentidade;
    private String noOrgaoEmissor;
    private Date dtEmissao;
    private String nuCpf;
    private String nuReservista;
    private Long inCertidao;
    private String nuCertidao;
    private String nuLivro;
    private String nuFolha;
    private String noCartorio;
    private Long edCidadeCartorio;
    private Long edUfCartorio;
    private Long nuTituloEleitor;
    private Long nuZonaEleitoral;
    private String nuSecao;
    private Long edUfTituloEleitoral;
    private Long inTipoEstadoCivil;
    private String noConjuge;
    private Long qtPeso;
    private Long qtAltura;
    private Long nuTempoEscolaridade;
    private String nuMatriculaAntiga;
    private Long inStatusAluno;
    private String txSenhaAcessaInternet;
    private String imFotoAluno;
    private String inStatusEnsinoReligioso;
    private String txInformacoesFichaMedica;
    private Long coLocalidadeNaturalidade;
    private String txLocalidadeNaturalidade;
    private Date dtOperacao;
    private String inOperacao;
    private Long coPacote;
    private Long coSeqSincronizacao;
    private String noResponsavel;
    private Integer coEtnia;
    private String ufNaturalidade;
    private String municipioNaturalidade;
    private String nuNis;
    private String noLogradouro;
    private String nuLogradouro;
    private String noBairro;
    private String nuCep;
    private String noLocalidade;
    private String noUf;
    private String listNecessidadeEspecial;
    
    public String getListNecessidadeEspecial() {
    	return this.listNecessidadeEspecial;
    }
    
    public String getSedListNecessidadeEspecial() {
    	if (this.listNecessidadeEspecial == null || this.listNecessidadeEspecial.isBlank()) {
    		return null;
    	}
    	
    	return String.join(";",
    		Arrays.asList(this.listNecessidadeEspecial.split(";")).stream()
    		.map(NecessidadesEspeciaisConverter::fromSge)
    		.collect(Collectors.toList()));
	}

	public void setListNecessidadeEspecial(String listNecessidadeEspecial) {
		this.listNecessidadeEspecial = listNecessidadeEspecial;
	}

	public String getNoLogradouro() {
		return noLogradouro;
	}

	public void setNoLogradouro(String noLogradouro) {
		this.noLogradouro = noLogradouro;
	}

	public String getNuLogradouro() {
		return nuLogradouro;
	}

	public void setNuLogradouro(String nuLogradouro) {
		this.nuLogradouro = nuLogradouro;
	}

	public String getNoBairro() {
		return noBairro;
	}

	public void setNoBairro(String noBairro) {
		this.noBairro = noBairro;
	}

	public String getNuCep() {
		return nuCep;
	}

	public void setNuCep(String nuCep) {
		this.nuCep = nuCep;
	}

	public String getNoLocalidade() {
		return noLocalidade;
	}

	public void setNoLocalidade(String noLocalidade) {
		this.noLocalidade = noLocalidade;
	}

	public String getNoUf() {
		return noUf;
	}

	public void setNoUf(String noUf) {
		this.noUf = noUf;
	}

	public String getNuNis() {
		return nuNis;
	}

	public void setNuNis(String nuNis) {
		this.nuNis = nuNis;
	}

	public String getMunicipioNaturalidade() {
		return municipioNaturalidade;
	}

	public void setMunicipioNaturalidade(String municipioNaturalidade) {
		this.municipioNaturalidade = municipioNaturalidade;
	}

	public String getUfNaturalidade() {
		return ufNaturalidade;
	}

	public void setUfNaturalidade(String ufNaturalidade) {
		this.ufNaturalidade = ufNaturalidade;
	}

	public String getNuMatricula() {
        return this.nuMatricula;
    }

    public Long getCoIdentificacaoAluno() {
        return this.coIdentificacaoAluno;
    }

    public Long getCoPaisNacionalidade() {
        return this.coPaisNacionalidade;
    }

    public Date getDtMatricula() {
        return this.dtMatricula;
    }

    public Date getDtIngresso() {
        return this.dtIngresso;
    }

    public String getNoAluno() {
        return this.noAluno;
    }

    public Long getInSexo() {
        return this.inSexo;
    }

    public Date getDtNascimento() {
        return this.dtNascimento;
    }

    public String getEdEmail() {
        return this.edEmail;
    }

    public Long getInSituacaoAcademica() {
        return this.inSituacaoAcademica;
    }

    public Long getInStatusAptoEducacaoFisica() {
        return this.inStatusAptoEducacaoFisica;
    }

    public String getNuIdentidade() {
        return this.nuIdentidade;
    }

    public String getNoOrgaoEmissor() {
        return this.noOrgaoEmissor;
    }

    public Date getDtEmissao() {
        return this.dtEmissao;
    }

    public String getNuCpf() {
        return this.nuCpf;
    }

    public String getNuReservista() {
        return this.nuReservista;
    }

    public Long getInCertidao() {
        return this.inCertidao;
    }

    public String getNuCertidao() {
        return this.nuCertidao;
    }

    public String getNuLivro() {
        return this.nuLivro;
    }

    public String getNuFolha() {
        return this.nuFolha;
    }

    public String getNoCartorio() {
        return this.noCartorio;
    }

    public Long getEdCidadeCartorio() {
        return this.edCidadeCartorio;
    }

    public Long getEdUfCartorio() {
        return this.edUfCartorio;
    }

    public Long getNuTituloEleitor() {
        return this.nuTituloEleitor;
    }

    public Long getNuZonaEleitoral() {
        return this.nuZonaEleitoral;
    }

    public String getNuSecao() {
        return this.nuSecao;
    }

    public Long getEdUfTituloEleitoral() {
        return this.edUfTituloEleitoral;
    }

    public Long getInTipoEstadoCivil() {
        return this.inTipoEstadoCivil;
    }

    public String getNoConjuge() {
        return this.noConjuge;
    }

    public Long getQtPeso() {
        return this.qtPeso;
    }

    public Long getQtAltura() {
        return this.qtAltura;
    }

    public Long getNuTempoEscolaridade() {
        return this.nuTempoEscolaridade;
    }

    public String getNuMatriculaAntiga() {
        return this.nuMatriculaAntiga;
    }

    public Long getInStatusAluno() {
        return this.inStatusAluno;
    }

    public String getTxSenhaAcessaInternet() {
        return this.txSenhaAcessaInternet;
    }

    public String getImFotoAluno() {
        return this.imFotoAluno;
    }

    public String getInStatusEnsinoReligioso() {
        return this.inStatusEnsinoReligioso;
    }

    public String getTxInformacoesFichaMedica() {
        return this.txInformacoesFichaMedica;
    }

    public Long getCoLocalidadeNaturalidade() {
        return this.coLocalidadeNaturalidade;
    }

    public String getTxLocalidadeNaturalidade() {
        return this.txLocalidadeNaturalidade;
    }

    public Date getDtOperacao() {
        return this.dtOperacao;
    }

    public String getInOperacao() {
        return this.inOperacao;
    }

    public Long getCoPacote() {
        return this.coPacote;
    }

    public Long getCoSeqSincronizacao() {
        return this.coSeqSincronizacao;
    }
    public void setNuMatricula(String nuMatricula) {
        this.nuMatricula = nuMatricula;
    }

    public void setCoIdentificacaoAluno(Long coIdentificacaoAluno) {
        this.coIdentificacaoAluno = coIdentificacaoAluno;
    }

    public void setCoPaisNacionalidade(Long coPaisNacionalidade) {
        this.coPaisNacionalidade = coPaisNacionalidade;
    }

    public void setDtMatricula(Date dtMatricula) {
        this.dtMatricula = dtMatricula;
    }

    public void setDtIngresso(Date dtIngresso) {
        this.dtIngresso = dtIngresso;
    }

    public void setNoAluno(String noAluno) {
        this.noAluno = noAluno;
    }

    public void setInSexo(Long inSexo) {
        this.inSexo = inSexo;
    }

    public void setDtNascimento(Date dtNascimento) {
        this.dtNascimento = dtNascimento;
    }

    public void setEdEmail(String edEmail) {
        this.edEmail = edEmail;
    }

    public void setInSituacaoAcademica(Long inSituacaoAcademica) {
        this.inSituacaoAcademica = inSituacaoAcademica;
    }

    public void setInStatusAptoEducacaoFisica(Long inStatusAptoEducacaoFisica) {
        this.inStatusAptoEducacaoFisica = inStatusAptoEducacaoFisica;
    }

    public void setNuIdentidade(String nuIdentidade) {
        this.nuIdentidade = nuIdentidade;
    }

    public void setNoOrgaoEmissor(String noOrgaoEmissor) {
        this.noOrgaoEmissor = noOrgaoEmissor;
    }

    public void setDtEmissao(Date dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public void setNuCpf(String nuCpf) {
        this.nuCpf = nuCpf;
    }

    public void setNuReservista(String nuReservista) {
        this.nuReservista = nuReservista;
    }

    public void setInCertidao(Long inCertidao) {
        this.inCertidao = inCertidao;
    }

    public void setNuCertidao(String nuCertidao) {
        this.nuCertidao = nuCertidao;
    }

    public void setNuLivro(String nuLivro) {
        this.nuLivro = nuLivro;
    }

    public void setNuFolha(String nuFolha) {
        this.nuFolha = nuFolha;
    }

    public void setNoCartorio(String noCartorio) {
        this.noCartorio = noCartorio;
    }

    public void setEdCidadeCartorio(Long edCidadeCartorio) {
        this.edCidadeCartorio = edCidadeCartorio;
    }

    public void setEdUfCartorio(Long edUfCartorio) {
        this.edUfCartorio = edUfCartorio;
    }

    public void setNuTituloEleitor(Long nuTituloEleitor) {
        this.nuTituloEleitor = nuTituloEleitor;
    }

    public void setNuZonaEleitoral(Long nuZonaEleitoral) {
        this.nuZonaEleitoral = nuZonaEleitoral;
    }

    public void setNuSecao(String nuSecao) {
        this.nuSecao = nuSecao;
    }

    public void setEdUfTituloEleitoral(Long edUfTituloEleitoral) {
        this.edUfTituloEleitoral = edUfTituloEleitoral;
    }

    public void setInTipoEstadoCivil(Long inTipoEstadoCivil) {
        this.inTipoEstadoCivil = inTipoEstadoCivil;
    }

    public void setNoConjuge(String noConjuge) {
        this.noConjuge = noConjuge;
    }

    public void setQtPeso(Long qtPeso) {
        this.qtPeso = qtPeso;
    }

    public void setQtAltura(Long qtAltura) {
        this.qtAltura = qtAltura;
    }

    public void setNuTempoEscolaridade(Long nuTempoEscolaridade) {
        this.nuTempoEscolaridade = nuTempoEscolaridade;
    }

    public void setNuMatriculaAntiga(String nuMatriculaAntiga) {
        this.nuMatriculaAntiga = nuMatriculaAntiga;
    }

    public void setInStatusAluno(Long inStatusAluno) {
        this.inStatusAluno = inStatusAluno;
    }

    public void setTxSenhaAcessaInternet(String txSenhaAcessaInternet) {
        this.txSenhaAcessaInternet = txSenhaAcessaInternet;
    }

    public void setImFotoAluno(String imFotoAluno) {
        this.imFotoAluno = imFotoAluno;
    }

    public void setInStatusEnsinoReligioso(String inStatusEnsinoReligioso) {
        this.inStatusEnsinoReligioso = inStatusEnsinoReligioso;
    }

    public void setTxInformacoesFichaMedica(String txInformacoesFichaMedica) {
        this.txInformacoesFichaMedica = txInformacoesFichaMedica;
    }

    public void setCoLocalidadeNaturalidade(Long coLocalidadeNaturalidade) {
        this.coLocalidadeNaturalidade = coLocalidadeNaturalidade;
    }

    public void setTxLocalidadeNaturalidade(String txLocalidadeNaturalidade) {
        this.txLocalidadeNaturalidade = txLocalidadeNaturalidade;
    }

    public void setDtOperacao(Date dtOperacao) {
        this.dtOperacao = dtOperacao;
    }

    public void setInOperacao(String inOperacao) {
        this.inOperacao = inOperacao;
    }

    public void setCoPacote(Long coPacote) {
        this.coPacote = coPacote;
    }

    public void setCoSeqSincronizacao(Long coSeqSincronizacao) {
        this.coSeqSincronizacao = coSeqSincronizacao;
    }
    
    

    public String getNoResponsavel() {
		return noResponsavel;
	}

	public void setNoResponsavel(String noResponsavel) {
		this.noResponsavel = noResponsavel;
	}
	
	public Integer getCoEtnia() {
		return coEtnia;
	}

	public void setCoEtnia(Integer coEtnia) {
		this.coEtnia = coEtnia;
	}

	public static AlunoSGE fromResultSet(ResultSet rs) throws SQLException {

        AlunoSGE obj = new AlunoSGE();

        obj.setNuMatricula(rs.getString(AlunoSGE.nuMatricula_));
        obj.setCoIdentificacaoAluno(rs.getLong(AlunoSGE.coIdentificacaoAluno_));
        obj.setCoPaisNacionalidade(rs.getLong(AlunoSGE.coPaisNacionalidade_));
        obj.setDtMatricula(rs.getDate(AlunoSGE.dtMatricula_));
        obj.setDtIngresso(rs.getDate(AlunoSGE.dtIngresso_));
        obj.setNoAluno(rs.getString(AlunoSGE.noAluno_));
        obj.setInSexo(rs.getLong(AlunoSGE.inSexo_));
        obj.setDtNascimento(rs.getDate(AlunoSGE.dtNascimento_));
        obj.setEdEmail(rs.getString(AlunoSGE.edEmail_));
        obj.setInSituacaoAcademica(rs.getLong(AlunoSGE.inSituacaoAcademica_));
        obj.setInStatusAptoEducacaoFisica(rs.getLong(AlunoSGE.inStatusAptoEducacaoFisica_));
        obj.setNuIdentidade(rs.getString(AlunoSGE.nuIdentidade_));
        obj.setNoOrgaoEmissor(rs.getString(AlunoSGE.noOrgaoEmissor_));
        obj.setDtEmissao(rs.getDate(AlunoSGE.dtEmissao_));
        obj.setNuCpf(rs.getString(AlunoSGE.nuCpf_));
        obj.setNuReservista(rs.getString(AlunoSGE.nuReservista_));
        obj.setInCertidao(rs.getLong(AlunoSGE.inCertidao_));
        obj.setNuCertidao(rs.getString(AlunoSGE.nuCertidao_));
        obj.setNuLivro(rs.getString(AlunoSGE.nuLivro_));
        obj.setNuFolha(rs.getString(AlunoSGE.nuFolha_));
        obj.setNoCartorio(rs.getString(AlunoSGE.noCartorio_));
        obj.setEdCidadeCartorio(rs.getLong(AlunoSGE.edCidadeCartorio_));
        obj.setEdUfCartorio(rs.getLong(AlunoSGE.edUfCartorio_));
        obj.setNuTituloEleitor(rs.getLong(AlunoSGE.nuTituloEleitor_));
        obj.setNuZonaEleitoral(rs.getLong(AlunoSGE.nuZonaEleitoral_));
        obj.setNuSecao(rs.getString(AlunoSGE.nuSecao_));
        obj.setEdUfTituloEleitoral(rs.getLong(AlunoSGE.edUfTituloEleitoral_));
        obj.setInTipoEstadoCivil(rs.getLong(AlunoSGE.inTipoEstadoCivil_));
        obj.setNoConjuge(rs.getString(AlunoSGE.noConjuge_));
        obj.setQtPeso(rs.getLong(AlunoSGE.qtPeso_));
        obj.setQtAltura(rs.getLong(AlunoSGE.qtAltura_));
        obj.setNuTempoEscolaridade(rs.getLong(AlunoSGE.nuTempoEscolaridade_));
        obj.setNuMatriculaAntiga(rs.getString(AlunoSGE.nuMatriculaAntiga_));
        obj.setInStatusAluno(rs.getLong(AlunoSGE.inStatusAluno_));
        obj.setTxSenhaAcessaInternet(rs.getString(AlunoSGE.txSenhaAcessaInternet_));
        obj.setImFotoAluno(rs.getString(AlunoSGE.imFotoAluno_));
        obj.setInStatusEnsinoReligioso(rs.getString(AlunoSGE.inStatusEnsinoReligioso_));
        obj.setTxInformacoesFichaMedica(rs.getString(AlunoSGE.txInformacoesFichaMedica_));
        obj.setCoLocalidadeNaturalidade(rs.getLong(AlunoSGE.coLocalidadeNaturalidade_));
        obj.setTxLocalidadeNaturalidade(rs.getString(AlunoSGE.txLocalidadeNaturalidade_));
        obj.setDtOperacao(rs.getDate(AlunoSGE.dtOperacao_));
        obj.setInOperacao(rs.getString(AlunoSGE.inOperacao_));
        obj.setCoPacote(rs.getLong(AlunoSGE.coPacote_));
        obj.setCoSeqSincronizacao(rs.getLong(AlunoSGE.coSeqSincronizacao_));
        obj.setNoResponsavel(rs.getString(AlunoSGE.coSeqSincronizacao_));
        obj.setCoEtnia(rs.getInt(AlunoSGE.coEtnia_));
        obj.setUfNaturalidade(rs.getString(AlunoSGE.ufNaturalidade_));
        obj.setMunicipioNaturalidade(rs.getString(AlunoSGE.municipioNaturalidade_));
        obj.setNuNis(rs.getString(AlunoSGE.nuNis_));
        obj.setNoLogradouro(rs.getString(AlunoSGE.noLogradouro_));
        obj.setNuLogradouro(rs.getString(AlunoSGE.nuLogradouro_));
        obj.setNoBairro(rs.getString(AlunoSGE.noBairro_));
        obj.setNuCep(rs.getString(AlunoSGE.nuCep_));
        obj.setNoLocalidade(rs.getString(AlunoSGE.noLocalidade_));
        obj.setNoUf(rs.getString(AlunoSGE.noUf_));
        obj.setListNecessidadeEspecial(rs.getString(AlunoSGE.listNecessidadeEspecial_));
        
        return obj;
    }
    
    
}