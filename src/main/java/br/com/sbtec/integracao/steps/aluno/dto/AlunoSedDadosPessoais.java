package br.com.sbtec.integracao.steps.aluno.dto;

import java.text.SimpleDateFormat;

import br.com.sbtec.integracao.steps.aluno.converter.EtniaCorRacaConverter;
import br.com.sbtec.integracao.steps.aluno.converter.InNascionalidadeUtil;
import br.com.sbtec.integracao.steps.aluno.converter.SexoConverter;

public class AlunoSedDadosPessoais {

	private String inNomeAluno;
	private String inNomeMae;
	private String inNomePai;
	private String inNomeSocial;
	private String inNomeAfetivo;
	private String inDataNascimento;
	private String inCorRaca;
	private String inSexo;
	private String inBolsaFamilia;
	private String inQuilombola;
	private String inPossuiInternet;
	private String inPossuiNotebookSmartphoneTablet;
	private String inTipoSanguineo;
	private String inDoadorOrgaos;
	private String inNumeroCNS;
	private String inEmail;
	private String inNacionalidade;
	private String inNomeMunNascto;
	private String inUFMunNascto;
	private String inCodMunNasctoDNE;
	private String inDataEntradaPais;
	private String inCodPaisOrigem;
	private String inPaisOrige;

	public static AlunoSedDadosPessoais fromSge(AlunoSGE alunoSge) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		AlunoSedDadosPessoais dp = new AlunoSedDadosPessoais();

		dp.setInNomeAluno(alunoSge.getNoAluno());
		dp.setInNomeMae(alunoSge.getNoResponsavel());
		dp.setInDataNascimento(sdf.format(alunoSge.getDtNascimento()));
		dp.setInCorRaca(EtniaCorRacaConverter.toSede(alunoSge.getCoEtnia()));
		dp.setInSexo(SexoConverter.toSede(alunoSge.getInSexo().intValue()));
		dp.setInEmail(alunoSge.getEdEmail());
		dp.setInNacionalidade(InNascionalidadeUtil.inNacionalidade(alunoSge.getCoPaisNacionalidade()));

		if (dp.getInNacionalidade().equals(InNascionalidadeUtil.inNacional)) {
			dp.setInNomeMunNascto(alunoSge.getMunicipioNaturalidade());
			dp.setInUFMunNascto(alunoSge.getUfNaturalidade());
			dp.setInCodPaisOrigem("76");
			dp.setInPaisOrige("Brasil");
		}

		if (!dp.getInNacionalidade().equals(InNascionalidadeUtil.inNacional)) {
//			inDataEntradaPais
		}

		dp.setInPossuiInternet("N");
		dp.setInPossuiNotebookSmartphoneTablet("N");

		return dp;
	}

	public String getInNomeAluno() {
		return inNomeAluno;
	}

	public void setInNomeAluno(String inNomeAluno) {
		this.inNomeAluno = inNomeAluno;
	}

	public String getInNomeMae() {
		return inNomeMae;
	}

	public void setInNomeMae(String inNomeMae) {
		this.inNomeMae = inNomeMae;
	}

	public String getInNomePai() {
		return inNomePai;
	}

	public void setInNomePai(String inNomePai) {
		this.inNomePai = inNomePai;
	}

	public String getInNomeSocial() {
		return inNomeSocial;
	}

	public void setInNomeSocial(String inNomeSocial) {
		this.inNomeSocial = inNomeSocial;
	}

	public String getInNomeAfetivo() {
		return inNomeAfetivo;
	}

	public void setInNomeAfetivo(String inNomeAfetivo) {
		this.inNomeAfetivo = inNomeAfetivo;
	}

	public String getInDataNascimento() {
		return inDataNascimento;
	}

	public void setInDataNascimento(String inDataNascimento) {
		this.inDataNascimento = inDataNascimento;
	}

	public String getInCorRaca() {
		return inCorRaca;
	}

	public void setInCorRaca(String inCorRaca) {
		this.inCorRaca = inCorRaca;
	}

	public String getInSexo() {
		return inSexo;
	}

	public void setInSexo(String inSexo) {
		this.inSexo = inSexo;
	}

	public String getInBolsaFamilia() {
		return inBolsaFamilia;
	}

	public void setInBolsaFamilia(String inBolsaFamilia) {
		this.inBolsaFamilia = inBolsaFamilia;
	}

	public String getInQuilombola() {
		return inQuilombola;
	}

	public void setInQuilombola(String inQuilombola) {
		this.inQuilombola = inQuilombola;
	}

	public String getInPossuiInternet() {
		return inPossuiInternet;
	}

	public void setInPossuiInternet(String inPossuiInternet) {
		this.inPossuiInternet = inPossuiInternet;
	}

	public String getInPossuiNotebookSmartphoneTablet() {
		return inPossuiNotebookSmartphoneTablet;
	}

	public void setInPossuiNotebookSmartphoneTablet(String inPossuiNotebookSmartphoneTablet) {
		this.inPossuiNotebookSmartphoneTablet = inPossuiNotebookSmartphoneTablet;
	}

	public String getInTipoSanguineo() {
		return inTipoSanguineo;
	}

	public void setInTipoSanguineo(String inTipoSanguineo) {
		this.inTipoSanguineo = inTipoSanguineo;
	}

	public String getInDoadorOrgaos() {
		return inDoadorOrgaos;
	}

	public void setInDoadorOrgaos(String inDoadorOrgaos) {
		this.inDoadorOrgaos = inDoadorOrgaos;
	}

	public String getInNumeroCNS() {
		return inNumeroCNS;
	}

	public void setInNumeroCNS(String inNumeroCNS) {
		this.inNumeroCNS = inNumeroCNS;
	}

	public String getInEmail() {
		return inEmail;
	}

	public void setInEmail(String inEmail) {
		this.inEmail = inEmail;
	}

	public String getInNacionalidade() {
		return inNacionalidade;
	}

	public void setInNacionalidade(String inNacionalidade) {
		this.inNacionalidade = inNacionalidade;
	}

	public String getInNomeMunNascto() {
		return inNomeMunNascto;
	}

	public void setInNomeMunNascto(String inNomeMunNascto) {
		this.inNomeMunNascto = inNomeMunNascto;
	}

	public String getInUFMunNascto() {
		return inUFMunNascto;
	}

	public void setInUFMunNascto(String inUFMunNascto) {
		this.inUFMunNascto = inUFMunNascto;
	}

	public String getInCodMunNasctoDNE() {
		return inCodMunNasctoDNE;
	}

	public void setInCodMunNasctoDNE(String inCodMunNasctoDNE) {
		this.inCodMunNasctoDNE = inCodMunNasctoDNE;
	}

	public String getInDataEntradaPais() {
		return inDataEntradaPais;
	}

	public void setInDataEntradaPais(String inDataEntradaPais) {
		this.inDataEntradaPais = inDataEntradaPais;
	}

	public String getInCodPaisOrigem() {
		return inCodPaisOrigem;
	}

	public void setInCodPaisOrigem(String inCodPaisOrigem) {
		this.inCodPaisOrigem = inCodPaisOrigem;
	}

	public String getInPaisOrige() {
		return inPaisOrige;
	}

	public void setInPaisOrige(String inPaisOrige) {
		this.inPaisOrige = inPaisOrige;
	}

}
