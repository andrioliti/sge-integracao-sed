package br.com.sbtec.integracao.steps.login;

import org.apache.http.HttpStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.sbtec.integracao.login.domain.LoginResp;
import br.com.sbtec.integracao.services.TokenService;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

@Component
public class LoginTasklet implements Tasklet {
	
	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		HttpResponse<String> resp = Unirest.get("/Usuario/ValidarUsuario")
			.basicAuth("SME207", "g74w5xy20")
			.asString();
		
		if (resp.getStatus() != HttpStatus.SC_OK) {
			throw new Exception(resp.getBody());
		}
		
		tokenService.setToken(objectMapper.readValue(resp.getBody(), LoginResp.class).getToken());

		return RepeatStatus.FINISHED;
	}

}
