package br.com.sbtec.integracao.steps.aluno.dto;

public class AlunoSed {

	private AlunoSedDadosPessoais inDadosPessoais;
	private AlunoSedEnderecoResidencial inEnderecoResidencial;
	private AlunoSedDocumentos inDocumentos;
	private AlunoSedCertidaoNova inCertidaoNova;
	private AlunoSedDeficiencia inDeficiencia;

	public AlunoSedDeficiencia getInDeficiencia() {
		return inDeficiencia;
	}

	public void setInDeficiencia(AlunoSedDeficiencia inDeficiencia) {
		this.inDeficiencia = inDeficiencia;
	}

	public AlunoSedEnderecoResidencial getInEnderecoResidencial() {
		return inEnderecoResidencial;
	}

	public void setInEnderecoResidencial(AlunoSedEnderecoResidencial inEnderecoResidencial) {
		this.inEnderecoResidencial = inEnderecoResidencial;
	}

	public AlunoSedDocumentos getInDocumentos() {
		return inDocumentos;
	}

	public void setInDocumentos(AlunoSedDocumentos inDocumentos) {
		this.inDocumentos = inDocumentos;
	}

	public AlunoSedCertidaoNova getInCertidaoNova() {
		return inCertidaoNova;
	}

	public void setInCertidaoNova(AlunoSedCertidaoNova inCertidaoNova) {
		this.inCertidaoNova = inCertidaoNova;
	}

	public AlunoSedDadosPessoais getInDadosPessoais() {
		return inDadosPessoais;
	}

	public void setInDadosPessoais(AlunoSedDadosPessoais inDadosPessoais) {
		this.inDadosPessoais = inDadosPessoais;
	}

	public static AlunoSed fromSGE(AlunoSGE alunoSge) {
		AlunoSed alunoSed = new AlunoSed();

		alunoSed.setInDadosPessoais(AlunoSedDadosPessoais.fromSge(alunoSge));
		AlunoSedCertidaoNova certidaoNova = AlunoSedCertidaoNova.fromSge(alunoSge);
		alunoSed.setInCertidaoNova(certidaoNova);
		alunoSed.setInDocumentos(AlunoSedDocumentos.fromSge(alunoSge, certidaoNova));
		alunoSed.setInEnderecoResidencial(AlunoSedEnderecoResidencial.fromSge(alunoSge));
		alunoSed.setInDeficiencia(AlunoSedDeficiencia.fromSge(alunoSge));
		
		return alunoSed;
	}

}
