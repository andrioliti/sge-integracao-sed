package br.com.sbtec.integracao.steps.aluno.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import br.com.sbtec.integracao.steps.aluno.dto.AlunoSGE;

@Component
public class AlunoRowMapper implements RowMapper<AlunoSGE> {

	@Override
	public AlunoSGE mapRow(ResultSet rs, int rowNum) throws SQLException {
		return AlunoSGE.fromResultSet(rs);
	}

}
