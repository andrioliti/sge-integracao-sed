package br.com.sbtec.integracao.steps.aluno.converter;

import java.util.Arrays;

public enum SexoConverter {
	M(2, "1"),
	F(1, "2");
	
	Integer sgeValue;
	String sedeValue;
	
	SexoConverter(Integer sge, String sede) {
		this.sgeValue = sge;
		this.sedeValue = sede;
	}
	
	public static String toSede(Integer sge) {
		return Arrays.asList(SexoConverter.values()).stream()
			.filter((SexoConverter it) -> it.sgeValue.equals(sge))
			.findFirst()
			.orElse(SexoConverter.M).sedeValue;
	}
	
}
