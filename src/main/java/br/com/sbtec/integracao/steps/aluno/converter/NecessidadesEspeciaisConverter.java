package br.com.sbtec.integracao.steps.aluno.converter;

import java.util.Arrays;

public enum NecessidadesEspeciaisConverter {
	MULTIPLA(1, -1),
	CEGUEIRA(2, 1),
	BAIXA_VISAO(3, 1),
	SURDEZ_SEVERA_OU_PROFUNDA(4, 2),
	SURDEZ_LEVE_OU_MODERADA(5, 2),
	SURDOCEGUEIRA(6, 3),
	FISICA_PARALISIA_CEREBRAL(7, 3),
	FISICA_CADEIRANTE(8, 3),
	FISICA_OUTROS(9, 3),
	SINDROME_DE_DOWN(10, 3),
	INTELECTUAL(11, 3),
	AUTISTA_INFANTIL(20, 3),
	SINDROME_DE_ASPERGER(21, 3),
	SINDROME_DE_RETT(22, 3),
	TRANSTORNO_DESINTEGRATIVO_DA_INFANCIA(23, 3),
	ALTAS_HABILIDADES_SUPERDOTACAO(30, 3),
	SEM_NECESSIDADES_ESPECIAIS(0,0);
	
	NecessidadesEspeciaisConverter(Integer sed, Integer sge) {
		this.sed = sed;
		this.sge = sge;
	} 
	
	public Integer sed;
	public Integer sge;
	
	public static String fromSge(String sge) {
		return Arrays.asList(NecessidadesEspeciaisConverter.values()).stream()
			.filter(it -> it.sed.equals(Integer.getInteger(sge)))
			.map(it -> it.sed.toString())
			.findFirst()
			.orElse("");
	}
}
