package br.com.sbtec.integracao.steps.aluno.dto;

public class AlunoSedUpdate {
	
	private AlunoSedInAluno inAluno;
	private AlunoSedDadosPessoais inDadosPessoais;
	private AlunoSedEnderecoResidencial inEnderecoResidencial;
	private AlunoSedDocumentos inDocumentos;
	private AlunoSedCertidaoNova inCertidaoNova;
	private AlunoSedDeficiencia inDeficiencia;

	public void fillInAluno(OutAluno out) {
		AlunoSedInAluno inAlunoP = new AlunoSedInAluno();
		
		inAlunoP.setInDigitoRA(out.getOutDigitoRA());
		inAlunoP.setInNumRA(out.getOutNumRA());
		inAlunoP.setInSiglaUFRA(out.getOutSiglaUFRA());
		
		this.setInAluno(inAlunoP);
	}
	
	public AlunoSedInAluno getInAluno() {
		return inAluno;
	}

	public void setInAluno(AlunoSedInAluno inAluno) {
		this.inAluno = inAluno;
	}

	public AlunoSedDeficiencia getInDeficiencia() {
		return inDeficiencia;
	}

	public void setInDeficiencia(AlunoSedDeficiencia inDeficiencia) {
		this.inDeficiencia = inDeficiencia;
	}

	public AlunoSedEnderecoResidencial getInEnderecoResidencial() {
		return inEnderecoResidencial;
	}

	public void setInEnderecoResidencial(AlunoSedEnderecoResidencial inEnderecoResidencial) {
		this.inEnderecoResidencial = inEnderecoResidencial;
	}

	public AlunoSedDocumentos getInDocumentos() {
		return inDocumentos;
	}

	public void setInDocumentos(AlunoSedDocumentos inDocumentos) {
		this.inDocumentos = inDocumentos;
	}

	public AlunoSedCertidaoNova getInCertidaoNova() {
		return inCertidaoNova;
	}

	public void setInCertidaoNova(AlunoSedCertidaoNova inCertidaoNova) {
		this.inCertidaoNova = inCertidaoNova;
	}

	public AlunoSedDadosPessoais getInDadosPessoais() {
		return inDadosPessoais;
	}

	public void setInDadosPessoais(AlunoSedDadosPessoais inDadosPessoais) {
		this.inDadosPessoais = inDadosPessoais;
	}

}
