package br.com.sbtec.integracao.steps.aluno.writer;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.sbtec.integracao.services.AlunoSgeRaService;
import br.com.sbtec.integracao.services.TokenService;
import br.com.sbtec.integracao.shared.OperationType;
import br.com.sbtec.integracao.steps.aluno.dto.AlunoSGE;
import br.com.sbtec.integracao.steps.aluno.dto.AlunoSed;
import br.com.sbtec.integracao.steps.aluno.dto.AlunoSedOutput;
import br.com.sbtec.integracao.steps.aluno.dto.AlunoSedUpdate;
import br.com.sbtec.integracao.steps.aluno.dto.AlunoSedUpdateOut;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

@Component
public class AlunoWriter implements ItemWriter<AlunoSGE>{

	@Autowired
	private ObjectMapper obm;
	
	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private AlunoSgeRaService raService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public void callApi(AlunoSGE aluno) {
		try {
			if (aluno.getInOperacao().equals(OperationType.I.getDb())) {
				//logica do insert
				HttpResponse<String> resInsert = Unirest.post("/Aluno/FichaAluno")
					.header("Authorization", tokenService.getToken())
					.body(obm.writeValueAsString(AlunoSed.fromSGE(aluno))).asString();
				
				if (resInsert.isSuccess()) {
					AlunoSedOutput out = obm.convertValue(resInsert.getBody(), AlunoSedOutput.class);
					
					if (out.getOutErro().isBlank()) {
						raService.setRa(out, aluno.getCoIdentificacaoAluno());
					}
				}
			} else {
				AlunoSedUpdate update = modelMapper.map(aluno, AlunoSedUpdate.class);
				
				AlunoSedOutput ra = raService.getFrom(aluno.getCoIdentificacaoAluno());
				update.fillInAluno(ra.getOutAluno());
				
				//logica do update
				HttpResponse<String> updateRes = Unirest.post("/Aluno/Manutencao")
					.header("Authorization", tokenService.getToken())
					.body(obm.writeValueAsString(AlunoSed.fromSGE(aluno)))
					.asString();
				
				if (updateRes.isSuccess()) {
					AlunoSedUpdateOut outUpdate = obm.convertValue(updateRes.getBody(), AlunoSedUpdateOut.class);
					
					if (!outUpdate.getOutErro().isBlank()) {
						System.err.println("Erro ao atualizar o registro " + aluno.getCoIdentificacaoAluno());
					}
				}
			}	
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void write(List<? extends AlunoSGE> items) throws Exception {
		items.stream()
			.forEach(this::callApi);
	}

}
