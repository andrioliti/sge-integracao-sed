package br.com.sbtec.integracao.steps.login;

import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoginTaskletConfig {
	
	@Bean("login")
	public Step buildStep(
			LoginTasklet step
			, StepBuilderFactory stepBuilderFactory
	) {
		return stepBuilderFactory.get("login-step")
				.tasklet(step)
				.build();
	}
}
