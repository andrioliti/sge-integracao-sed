package br.com.sbtec.integracao.steps.aluno;

import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.sbtec.integracao.steps.aluno.dto.AlunoSGE;
import br.com.sbtec.integracao.steps.aluno.reader.AlunoReader;
import br.com.sbtec.integracao.steps.aluno.writer.AlunoWriter;

@Configuration
public class AlunoStepConfig {

	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	private AlunoReader alunoReader;
	
	@Autowired
	private AlunoWriter alunoWriter;
	
	@Bean("aluno-step")
	public Step getStep() {
		return stepBuilderFactory.get("aluno-step")
				.<AlunoSGE, AlunoSGE>chunk(10)
				.reader(alunoReader.getReader())
				.writer(alunoWriter)
				.build();
	}
}
