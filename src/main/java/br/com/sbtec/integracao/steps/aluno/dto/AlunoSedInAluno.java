package br.com.sbtec.integracao.steps.aluno.dto;

public class AlunoSedInAluno {
	private String inNumRA;
	private String inDigitoRA;
	private String inSiglaUFRA;

	public String getInNumRA() {
		return inNumRA;
	}

	public void setInNumRA(String inNumRA) {
		this.inNumRA = inNumRA;
	}

	public String getInDigitoRA() {
		return inDigitoRA;
	}

	public void setInDigitoRA(String inDigitoRA) {
		this.inDigitoRA = inDigitoRA;
	}

	public String getInSiglaUFRA() {
		return inSiglaUFRA;
	}

	public void setInSiglaUFRA(String inSiglaUFRA) {
		this.inSiglaUFRA = inSiglaUFRA;
	}

}
