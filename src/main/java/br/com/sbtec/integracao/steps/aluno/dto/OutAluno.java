package br.com.sbtec.integracao.steps.aluno.dto;

public class OutAluno {

	private String outNumRA;
	private String outDigitoRA;
	private String outSiglaUFRA;

	public String getOutNumRA() {
		return outNumRA;
	}

	public void setOutNumRA(String outNumRA) {
		this.outNumRA = outNumRA;
	}

	public String getOutDigitoRA() {
		return outDigitoRA;
	}

	public void setOutDigitoRA(String outDigitoRA) {
		this.outDigitoRA = outDigitoRA;
	}

	public String getOutSiglaUFRA() {
		return outSiglaUFRA;
	}

	public void setOutSiglaUFRA(String outSiglaUFRA) {
		this.outSiglaUFRA = outSiglaUFRA;
	}

}
