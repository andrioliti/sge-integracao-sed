package br.com.sbtec.integracao.steps.aluno.dto;

public class AlunoSedUpdateOut {
	private String outSucesso;
	private String outErro;
	private String outProcessoID;

	public String getOutSucesso() {
		return outSucesso;
	}

	public void setOutSucesso(String outSucesso) {
		this.outSucesso = outSucesso;
	}

	public String getOutErro() {
		return outErro;
	}

	public void setOutErro(String outErro) {
		this.outErro = outErro;
	}

	public String getOutProcessoID() {
		return outProcessoID;
	}

	public void setOutProcessoID(String outProcessoID) {
		this.outProcessoID = outProcessoID;
	}

}
