package br.com.sbtec.integracao.steps.aluno.dto;

public class AlunoSedOutput {

	private OutAluno outAluno;
	private String outSucesso;
	private String outErro;
	private Long outProcessoID;

	public OutAluno getOutAluno() {
		return outAluno;
	}

	public void setOutAluno(OutAluno outAluno) {
		this.outAluno = outAluno;
	}

	public String getOutSucesso() {
		return outSucesso;
	}

	public void setOutSucesso(String outSucesso) {
		this.outSucesso = outSucesso;
	}

	public String getOutErro() {
		return outErro;
	}

	public void setOutErro(String outErro) {
		this.outErro = outErro;
	}

	public Long getOutProcessoID() {
		return outProcessoID;
	}

	public void setOutProcessoID(Long outProcessoID) {
		this.outProcessoID = outProcessoID;
	}

}
