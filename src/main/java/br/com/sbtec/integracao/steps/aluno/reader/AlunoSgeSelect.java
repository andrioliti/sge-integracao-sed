package br.com.sbtec.integracao.steps.aluno.reader;

public abstract class AlunoSgeSelect {
	public static String SELECT = "select \n" + 
			"	f.*,\n" + 
			"	a.dt_operacao,\n" + 
			"	a.in_operacao,\n" + 
			"	a.co_pacote,\n" + 
			"	a.co_seq_sincronizacao,\n" + 
			"	e.no_responsavel,\n" + 
			"	h.no_uf as uf_naturalidade,\n" + 
			"	g.no_localidade as municipio_naturalidade,\n" + 
			"	m.no_logradouro,\n" + 
			"	j.nu_logradouro,\n" + 
			"	o.no_bairro,\n" + 
			"	p.nu_cep,\n" + 
			"	n.no_localidade,\n" + 
			"	l.no_uf,\n" + 
			"	u.list_necessidade_especial\n" + 
			"from  \n" + 
			"	SIN_ALUNO a\n" + 
			"inner join \n" + 
			"	ITW_RESPONSAVEL_ALUNO d\n" + 
			"	on (d.co_identificacao_aluno = a.co_identificacao_aluno and d.co_tipo_responsavel = 1) \n" + 
			"inner join\n" + 
			"	ITW_RESPONSAVEL e\n" + 
			"	on e.co_responsavel = d.co_responsavel\n" + 
			"inner join\n" + 
			"	ITW_ALUNO f\n" + 
			"	on f.co_identificacao_aluno = a.co_identificacao_aluno\n" + 
			"inner join\n" + 
			"	ITW_LOCALIDADE g\n" + 
			"	on g.co_localidade = a.co_localidade_naturalidade\n" + 
			"inner join\n" + 
			"	ITW_UF h\n" + 
			"	on h.co_uf = g.co_uf\n" + 
			"inner join\n" + 
			"	ITW_ALUNO_POSSUI_ENDERECO i\n" + 
			"	on f.co_identificacao_aluno = i.co_identificacao_aluno \n" + 
			"inner join\n" + 
			"	ITW_ENDERECO j\n" + 
			"	on (i.co_endereco = j.co_endereco  and j.in_tipo_endereco = 1)\n" + 
			"inner join\n" + 
			"	ITW_UF l\n" + 
			"	on l.co_uf = j.co_uf\n" + 
			"inner join \n" + 
			"	ITW_LOGRADOURO m\n" + 
			"	on j.co_logradouro = m.co_logradouro\n" + 
			"inner join\n" + 
			"	ITW_LOCALIDADE n\n" + 
			"	on j.co_localidade = n.co_localidade\n" + 
			"inner join\n" + 
			"	ITW_BAIRRO o\n" + 
			"	on j.co_bairro = o.co_bairro\n" + 
			"inner join\n" + 
			"	ITW_COD_ENDERECAMENTO_POSTAL p\n" + 
			"	on j.co_cep = p.co_cep\n" + 
			"left join \n" + 
			"	(select \n" + 
			"			f.co_identificacao_aluno,\n" + 
			"			STRING_AGG(r.co_necessidade_problema_saude, ';') as list_necessidade_especial\n" + 
			"		from \n" + 
			"			ITW_ALUNO f\n" + 
			"		inner join\n" + 
			"			ITW_ALUNO_POSSUI_NECES_PROBLEM q\n" + 
			"			on f.co_identificacao_aluno = q.co_identificacao_aluno\n" + 
			"		inner join\n" + 
			"			ITW_TIPO_NECESSIDADE_PROB_SAUD r\n" + 
			"			on q.co_necessidade_problema_saude = r.co_necessidade_problema_saude\n" + 
			"		where \n" + 
			"			q.co_necessidade_problema_saude <= 10\n" + 
			"		group by\n" + 
			"			f.co_identificacao_aluno\n" + 
			"	) as u\n" + 
			"	on u.co_identificacao_aluno = f.co_identificacao_aluno\n" + 
			"where \n" + 
			"	a.dt_operacao >= cast('2021-01-01' as date) \n" + 
			"	and a.co_identificacao_aluno  not in (\n" + 
			"		select co_identificacao_aluno from sin_ALUNO where in_operacao = 'D'\n" + 
			"	)\n" + 
			"order by\n" + 
			"	a.dt_operacao asc";
}
