package br.com.sbtec.integracao.shared;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class DateUtil {

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	public static String formatDefault(Date date) {
		return DateUtil.sdf.format(date);
	}
}
