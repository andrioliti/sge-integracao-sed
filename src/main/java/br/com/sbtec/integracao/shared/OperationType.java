package br.com.sbtec.integracao.shared;

public enum OperationType {

	U("U", "update"), I("I", "insert"), D("D", "delete"); 
	
	String db;
	String full;
	
	OperationType(String db, String full) {
		this.db = db;
		this.full = full;
	}

	public String getDb() {
		return db;
	}

	public String getFull() {
		return full;
	}
	
	public Boolean compareDb(String v) {
		return this.db == v;
	}
	
	public Boolean compareFull(String v) {
		return this.full == v;
	}
}
