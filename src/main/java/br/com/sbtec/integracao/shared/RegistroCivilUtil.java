package br.com.sbtec.integracao.shared;

import br.com.sbtec.integracao.steps.aluno.dto.AlunoSedDocumentos;

public abstract class RegistroCivilUtil {

	public static String getUfFromEmissor(String emissor) {
		if (emissor != null && emissor.isBlank()) {
			return emissor.split("-")[1];
		}
		
		return null;
	}

	public static Boolean isDigito(AlunoSedDocumentos docs) {
		return Integer.valueOf(docs.getInNumDoctoCivil()) >= 24000000 && docs.getInUFDoctoCivil().equals("SP");
	}
}
