package br.com.sbtec.integracao.login.domain;

public class LoginResp {
	private String outAutenticacao;
	private String outUsuario;
	private String outErro;
	private Integer outProcessoID;

	public String getOutAutenticacao() {
		return outAutenticacao;
	}

	public void setOutAutenticacao(String outAutenticacao) {
		this.outAutenticacao = outAutenticacao;
	}

	public String getOutUsuario() {
		return outUsuario;
	}

	public void setOutUsuario(String outUsuario) {
		this.outUsuario = outUsuario;
	}

	public String getOutErro() {
		return outErro;
	}

	public void setOutErro(String outErro) {
		this.outErro = outErro;
	}

	public Integer getOutProcessoID() {
		return outProcessoID;
	}

	public void setOutProcessoID(Integer outProcessoID) {
		this.outProcessoID = outProcessoID;
	}
	
	public String getToken() {
		return outAutenticacao;
	}

}
