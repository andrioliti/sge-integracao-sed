package br.com.sbtec.integracao.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.sbtec.integracao.steps.aluno.dto.AlunoSedOutput;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class AlunoSgeRaService {

	private Map<Long, AlunoSedOutput> map = new HashMap<>();
	
	public void setRa(AlunoSedOutput output, Long codigoAluno) {
		map.put(codigoAluno, output);
	}
	
	public AlunoSedOutput getFrom(Long codigoAluno) {
		return map.get(codigoAluno);
	}
}
