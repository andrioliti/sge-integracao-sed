package br.com.sbtec.integracao.services;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class TokenService {

	private String token;

	public String getToken() {
		return token;
	}
	
	public String getFormatedToken() {
		return new StringBuilder("Bearer ")
				.append(token)
				.toString();
	}

	public void setToken(String token) {
		this.token = token;
	}

}
